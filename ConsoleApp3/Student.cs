﻿using System;

namespace ConsoleApp3
{
    public class Student
    {
       public string hoten;
       public double toan, ly, hoa;
       public string Hoten
       {
           get => hoten;
           set => hoten = value;
       }
       public double Toan
       {
           get => toan;
           set => toan = value;
       }
       public double Ly
       {
           get => ly;
           set => ly = value;
       }
       public double Hoa
       {
           get => hoa;
           set => hoa = value;
       }
       public void nhap()
       {
           Console.WriteLine("Nhap ho ten:");
           hoten = Console.ReadLine();
           Console.WriteLine("Nhap diem toan:");
           toan = (double)Convert.ToDouble(Console.ReadLine());
           Console.WriteLine("Nhap diem ly:");
           ly = (double)Convert.ToDouble(Console.ReadLine());
           Console.WriteLine("Nhap diem hoa");
           hoa = (double)Convert.ToDouble(Console.ReadLine());
       }
       public double DTB(double toan,double ly,double hoa)
       {
           return ((toan + ly + hoa) / 3);
       }
   }
}