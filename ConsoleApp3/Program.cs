﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            int sosv = 0;
            Console.WriteLine("Nhap so sinh vien");
            do
            {
                sosv = Convert.ToInt32(Console.ReadLine());
                if (sosv <= 0)
                {
                    Console.WriteLine("Error");
                    Console.WriteLine("Nhap so sinh vien");
                }
                else break;
            } while (sosv > 0);

            List<Student> listStudent = new List<Student>();
            for (int i = 0; i < sosv; i++)
            {
                Student student = new Student();
                student.nhap();
                listStudent.Add(student);
            }

            var listStudentOrder = listStudent.OrderByDescending(a => a.DTB(a.toan, a.ly, a.hoa)).ToList();
            if (sosv >= 3)
            {
                Console.WriteLine("3 sinh vien co diem trung binh sap xep giam dan: ");
                for (int i = 0; i < 3; i++)
                {
                    Console.WriteLine("stt {0} || ten {1} || DTB {2}", (i + 1), listStudentOrder[i].hoten,
                        listStudentOrder[i].DTB(listStudentOrder[i].toan, listStudentOrder[i].ly,
                            listStudentOrder[i].hoa));
                }
            }
            else if (sosv < 3)
            {
                for (int i = 0; i < sosv; i++)
                {
                    Console.WriteLine("stt {0} || ten {1} || DTB {2}", (i + 1), listStudentOrder[i].hoten,
                        listStudentOrder[i].DTB(listStudentOrder[i].toan, listStudentOrder[i].ly,
                            listStudentOrder[i].hoa));
                }
            }
            else
            {
                Console.WriteLine("Error");
            }

            Console.ReadKey();
        }
    }
}